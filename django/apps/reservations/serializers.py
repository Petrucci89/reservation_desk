from rest_framework import serializers
from .models import Reservation, MeetingRoom
from apps.staff.serializers import EmployeeSerializer


class MeetingRoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = MeetingRoom
        fields = (
            'id',
            'name'
        )


class ReservationCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reservation
        fields = (
            'id',
            'created_by',
            'title',
            'meeting_room',
            'from_dt',
            'to_dt',
            'employees'
        )

        extra_kwargs = {
            'id': {'read_only': True},
            'created_by': {'read_only': True},
        }


class ReservationInfoSerializer(serializers.ModelSerializer):

    created_by = EmployeeSerializer()
    meeting_room = MeetingRoomSerializer()

    class Meta:
        model = Reservation
        fields = (
            'id',
            'title',
            'meeting_room',
            'from_dt',
            'to_dt',
            'employees',
            'created_by',
            'created_at'
        )

