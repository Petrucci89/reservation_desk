from .serializers import EmployeeLoginSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import login, logout
from apps.utils.auth_classes import CsrfExemptSessionAuthentication

class LoginUser(APIView):

    serializer_class = EmployeeLoginSerializer
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.validated_data['user']

        if not user:
            return Response({"detail": "Auth error"}, status=status.HTTP_401_UNAUTHORIZED)

        login(request, user)

        return Response(status=status.HTTP_200_OK)


class LogoutUser(APIView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return Response(status=status.HTTP_200_OK)
