# Launch instruction

**1. Clone repository to your directory**
`git clone https://petrucci89@bitbucket.org/petrucci89/reservation_desk.git`

**2. Create .env file with fields**

SECRET_KEY=DJANGO SECRET KEY
POSTGRES_DB=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=YOUR PASSWORD
POSTGRES_HOST=db
POSTGRES_PORT=5432

**3. Install Docker and Docker-Compose**

**4. Run commands**
```bash
docker-compose build
docker-compose run django python manage.py collectstatic
docker-compose run django python manage.py makemigraions
docker-compose run django python manage.py migrate
docker-compose run django python manage.py createsuperuser if you need django admin
```
**5. Run application**
`docker-compose down && docker-compose up -d`
