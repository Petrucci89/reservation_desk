from django.db import models
from django.contrib.auth.models import User

class Employee(User):

    class Meta:
        proxy = True
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'
