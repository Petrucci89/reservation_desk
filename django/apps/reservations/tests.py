from django.urls import reverse
from rest_framework.test import APITestCase
from apps.reservations.models import Reservation, MeetingRoom
from apps.staff.models import Employee
from rest_framework import status
from django.utils import timezone
from datetime import timedelta
from .serializers import ReservationInfoSerializer

class EmployeeTests(APITestCase):

    def test_get_list_reservations(self):

        employee1 = Employee.objects.create_user(username='Mark', password='111')
        employee2 = Employee.objects.create_user(username='Sara', password='222')
        meeting_room = MeetingRoom.objects.create(name='room1')
        now_dt = timezone.now()
        from_dt = now_dt
        to_dt = from_dt + timedelta(hours=1)
        reservation1 = Reservation.objects.create(title='1',
                                                  meeting_room=meeting_room,
                                                  from_dt=from_dt,
                                                  to_dt=to_dt,
                                                  created_by=employee1,
                                                  created_at=now_dt)
        reservation1.employees.add(employee1)
        reservation1.employees.add(employee2)

        url = reverse('reservations:list')
        response = self.client.get(url)
        serialized_data = ReservationInfoSerializer([reservation1], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serialized_data, response.data)

    def test_create_reservations(self):

        employee1 = Employee.objects.create_user(username='Mark', password='111')
        employee2 = Employee.objects.create_user(username='Sara', password='222')
        meeting_room = MeetingRoom.objects.create(name='room1')
        now_dt = timezone.now()
        from_dt = now_dt
        to_dt = from_dt + timedelta(hours=1)

        self.client.login(username='Mark', password='111')

        data = {
            "title": '1',
            "meeting_room": meeting_room.id,
            "from_dt": from_dt,
            "to_dt": to_dt,
            "employees": [
                employee1.id,
                employee2.id
            ]
        }

        url = reverse('reservations:list')
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_cancel_reservations(self):

        employee1 = Employee.objects.create_user(username='Mark', password='111')
        employee2 = Employee.objects.create_user(username='Sara', password='222')
        meeting_room = MeetingRoom.objects.create(name='room1')
        now_dt = timezone.now()
        from_dt = now_dt
        to_dt = from_dt + timedelta(hours=1)
        reservation1 = Reservation.objects.create(title='1',
                                                  meeting_room=meeting_room,
                                                  from_dt=from_dt,
                                                  to_dt=to_dt,
                                                  created_by=employee1,
                                                  created_at=now_dt)
        reservation1.employees.add(employee1)
        reservation1.employees.add(employee2)

        url = reverse('reservations:detail', kwargs={'pk': 1})
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)

