from rest_framework import serializers
from .models import Employee
from django.contrib.auth import authenticate


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = (
            'id',
            'username'
        )


class EmployeeLoginSerializer(serializers.Serializer):
    login = serializers.CharField(required=True)
    password = serializers.CharField(
        trim_whitespace=False,
        required=True
    )

    def validate(self, attrs):
        login = attrs.get('login')
        password = attrs.get('password')

        if login and password:
            user = authenticate(username=login, password=password)
        else:
            msg = "login and password are required."
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
