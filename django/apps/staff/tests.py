from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase
from apps.staff.models import Employee
from rest_framework import status

# Create your tests here.

class EmployeeTests(APITestCase):

    def setUp(self):
        Employee.objects.create_user(username='Mark', password='qweasdzxc')

    def test_login(self):
        url = reverse('staff:login')
        response = self.client.post(url, {'login': 'Mark', 'password': 'qweasdzxc'}, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_logout(self):
        url = reverse('staff:logout')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

