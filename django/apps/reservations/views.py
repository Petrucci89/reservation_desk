from .models import Reservation

from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from .filters import ReservationFilter

from apps.utils.auth_classes import CsrfExemptSessionAuthentication

from .serializers import ReservationCreateSerializer, ReservationInfoSerializer

import logging
logging.basicConfig(filename="./log.txt", level=logging.ERROR)
# Create your views here.


class ReservationViewSet(viewsets.ModelViewSet):

    queryset = Reservation.objects.all()
    authentication_classes = (CsrfExemptSessionAuthentication, )
    permission_classes = [IsAuthenticated]
    serializer_class = ReservationInfoSerializer
    filterset_class = ReservationFilter

    def create(self, request, *args, **kwargs):
        self.serializer_class = ReservationCreateSerializer
        return super().create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

