from django.db import models

from apps.staff.models import Employee


class MeetingRoom(models.Model):

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'MeetingRoom'
        verbose_name_plural = 'MeetingRooms'


class Reservation(models.Model):

    title = models.CharField(max_length=255)
    meeting_room = models.ForeignKey(MeetingRoom, on_delete=models.CASCADE, related_name='meeting_room_reservations')
    from_dt = models.DateTimeField()
    to_dt = models.DateTimeField()
    employees = models.ManyToManyField(Employee)
    created_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='employee_reservations')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.meeting_room.name}: {self.from_dt} - {self.to_dt}"

    class Meta:
        verbose_name = 'Reservation'
        verbose_name_plural = 'Reservations'
