from django_filters import rest_framework as filters
from .models import Reservation


class ReservationFilter(filters.FilterSet):

    class Meta:
        model = Reservation
        fields = ['meeting_room', 'created_by', 'from_dt', 'to_dt', ]
