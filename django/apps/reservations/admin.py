from django.contrib import admin
from .models import Reservation, MeetingRoom


@admin.register(MeetingRoom)
class MeetingRoomAdmin(admin.ModelAdmin):
    list_display = ('name', )


@admin.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    list_display = ('created_by', 'meeting_room', 'from_dt', 'to_dt', )
