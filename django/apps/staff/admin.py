from django.contrib import admin
from .models import Employee
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

@admin.register(Employee)
class EmployeeAdmin(BaseUserAdmin):
    list_display = ('username', 'date_joined', )
