from django.urls import path
from .views import ReservationViewSet

app_name = 'apps.reservations'

reservation_list = ReservationViewSet.as_view({
    'get': 'list',
    'post': 'create',
})

reservation_detail = ReservationViewSet.as_view({
    'get': 'retrieve',
    'delete': 'destroy',
})

urlpatterns = [

    path('', reservation_list, name='list'),
    path('<int:pk>/', reservation_detail, name='detail'),

]
